//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjson = require('request-json');
var path = require('path');

var bodyparser = require('body-parser');
  app.use(bodyparser.json());

  // Para Clientes MLAB inicia
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  // Para Clientes MLAB termina



var movimientosV2JSON = require('./movimientosv2.json');

// Para Clientes MLAB inicia

var urlClientesMLab = "https://api.mlab.com/api/1/databases/mnajera/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlClientesMLab);

var urlTarifasMLab = "https://api.mlab.com/api/1/databases/mnajera/collections/Tarifas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var tarifaMLab = requestjson.createClient(urlTarifasMLab);

// Para Clientes MLAB termina

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

// ejemplo de peticion: get, post, put y delete
app.get('/', function(req,res) {
  // res.send('Hemos recibido su petición GET');
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function(req, res) {
  res.send ('Hemos recibido su petición por POST')
})

app.put('/', function(req, res) {
  res.send ('Hemos recibido su petición por PUT')
})

app.delete('/', function(req, res) {
  res.send ('Hemos recibido su petición por DELETE')
})

// en otra ruta: get, post, put y delete
app.get('/v1/Clientes/:idcliente', function(req,res) {
  res.send('Aqui tiene al cliente número: ' + req.params.idcliente);
})

app.get('/v1/movimientos', function(req,res) {
  res.sendfile('movimientos.json');
})

app.get('/v2/movimientos', function(req,res) {
  res.json(movimientosV2JSON);
})

app.get('/v2/movimientos/:index', function(req,res) {
  console.log(req.params.index);
  res.send(movimientosV2JSON[req.params.index-1]);
})

app.get('/v2/movimientosquery', function(req,res) {
  console.log(req.query);
  res.send('recibido');
})

// Insertar un nuevo registro:
app.post('/v2/movimientos', function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosV2JSON.length + 1
  movimientosV2JSON.push(nuevo)
  res.send('movimiento dado de alta: ' + nuevo.id)
})

// Falta hacer post, put y delete
app.post('/v1/Clientes/:idcliente', function(req,res) {
  res.send('Aqui tiene POST del cliente número: ' + req.params.idcliente);
})

app.put('/v2/movimientos', function(req, res) {
  res.send('Hemos recibido su petición put de movimientos');
})

// para trabajar con Clientes MLAB inicia
app.get('/v3/Clientes', function(req, res){
  clienteMLab.get('', function(err, resM, body){
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.post('/v3/Clientes', function(req, res) {
  clienteMLab.post('',req.body, function(err, resM, body) {
    res.send(body);
  })
})

app.get('/v3/Tarifas', function(req, res){
  tarifaMLab.get('', function(err, resM, body){
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

// para trabajar con Clientes MLAB termina
